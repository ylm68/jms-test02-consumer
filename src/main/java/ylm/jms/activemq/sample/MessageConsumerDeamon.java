package ylm.jms.activemq.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pleroux on 18/12/2014.
 */
public class MessageConsumerDeamon extends Thread {

  private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumerDeamon.class);

  private boolean running;

  public MessageConsumerDeamon() {
    super("Initialisation Message Consumer Deamon");
  }

  public void initialize() {
    LOGGER.info("MessageConsumerDeamon : OK");
  }

  public static void main(String args[]) {

    if (args.length > 1) {
      System.err.println("Usage: MessageConsumerDeamon [command]");
      LOGGER.info("Usage: MessageConsumerDeamon [command]");
      return;
    }
    MessageConsumerDeamon demon = new MessageConsumerDeamon();
    demon.initialize();
    demon.start();
  }

  public void run() {

    running = true;
    while (running) {
    }
  }

  public void stopServer() {
    running = false;
  }

}
