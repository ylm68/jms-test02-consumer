package ylm.jms.activemq.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import ylm.jms.activemq.sample.dto.PersonneDto;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by ylm68 on 11/12/2014.
 */
@Component("PersonneMessageConsumer")
public class MessageConsumer implements javax.jms.MessageListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);
  final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

  public static void main(String[] args) throws ParseException {

    LOGGER.info("Chargement configuration spring...");
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    LOGGER.info("Chargement configuration spring OK");
  }

  public void onMessage(Message message) {

    if (message instanceof ObjectMessage) {
      ObjectMessage objectMessage = (ObjectMessage) message;
      try {
          PersonneDto personne = (PersonneDto) objectMessage.getObject();
        LOGGER.info("message reçu : [" + personne.getPrenom() + " " + personne.getPrenom() + " né le " + dateFormat.format(personne.getDateDeNaissance()) + "]");
      } catch (JMSException e) {
        LOGGER.warn("Exception : [" + e.getMessage() + "]");
      }
    }
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      LOGGER.warn("Exception : [" + e.getMessage() + "]");
    }
  }

}
